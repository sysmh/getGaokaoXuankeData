package getGaokaoXuankeData.controller;

import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

/**
 * 操作word
 */
public class WordUtils {
    public static void main(String[] args) throws IOException {
        read_word();
    }

    //写入word
    public static void read_word() throws IOException {
        XWPFDocument xdoc = new XWPFDocument();
        // 创建一个段落
        XWPFParagraph xpara = xdoc.createParagraph();
        // 一个XWPFRun代表具有相同属性的一个区域。
        XWPFRun run = xpara.createRun();
        for(int i=0;i<100;i++){
            run.setBold(true); // 加粗
            run.setText("加粗的内容");
            run = xpara.createRun();
            run.setColor("FF0000");
            run.setFontSize(15);
            run.setText("插入内容。");
        }
        OutputStream os = new FileOutputStream("F:\\aloha.docx");
        xdoc.write(os);
        os.close();
    }
}
